import sys

def get_text_and_labels(txt_fn, out_fn):
    with open(txt_fn, 'r') as fin, open(out_fn, 'w') as fout:
        fout.write("tokens,labels\n")
        tokens, labels = [], []
        for line in fin:
            if len(line.strip()) == 0:
                fout.write("{},{}\n".format(" ".join(tokens), " ".join(labels)))
                tokens, labels = [], []
            else:
                line = line.strip().split()
                assert len(line) == 4
                token = line[0]
                label = line[-1]
                if token == ",":
                    token = "X"
                if token == "..." or token == "....":
                    continue
                if token == "\"":
                    continue
                tokens.append(token.replace(',', ''))
                labels.append(label)

txt_fn = "data/eng.train.txt"
get_text_and_labels(txt_fn, "data/train.csv")

txt_fn = "data/eng.testa.txt"
get_text_and_labels(txt_fn, "data/dev.csv")

txt_fn = "data/eng.testb.txt"
get_text_and_labels(txt_fn, "data/test.csv")

# README #

Exaplains how to train the NER model using transformers repo


### Step1 ###
* Need google account to train the model
	* open a new Google Colab file 
    * Choose the GPU as runtime

### Step2 ###
* Run the following commands in each cell
	* from google.colab import drive
    * drive.mount('/content/drive') (it requires the authentication and follow the steps on the screen)
    * !git clone https://sgangireddy@bitbucket.org/sgangireddy/ner.git
    * %cd ner/transformers/
    * !pip install .
    * !pip install -r examples/pytorch/token-classification/requirements.txt
    * from transformers import AutoModelForTokenClassification, AutoTokenizer
    * import torch
    * !python examples/pytorch/token-classification/run_ner.py --model_name_or_path bert-base-cased --train_file  "/content/ner/data/train.csv" --validation_file "/content/ner/data/dev.csv" --text_column_name "tokens" --label_column_name "labels" --output_dir "/content/ner/bert-base-cased-finetune-ner" --do_train --do_eval
